const fs = require('fs');
const chalk = require('chalk');

const removeNote = (title) => {
    const notes = loadNotes();

    const notesToKeep = notes.filter((note) => note.title !== title);

    if (notes.length > notesToKeep.length) {
        console.log(chalk.green.inverse('Note removed!'));
        saveNotes(notesToKeep);
    } else {
        console.log(chalk.red.inverse('No note found!'));
    }
};

const addNote = (title, body) => {
    const notes = loadNotes();
    const duplicateNote = notes.find((note) => note.title === title);

    if(!duplicateNote) {
        notes.push({
            title: title,
            body: body
        });
    
        saveNotes(notes);
        console.log(chalk.green.inverse('New note added!'));
    } else {
        console.log(chalk.red.inverse('Note title taken!'));
    }
}; 


const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes);
    fs.writeFileSync('notes.json', dataJsON);
};

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    }
    catch(e) {
        return [];
    }
};


const listNotes = () => {
    const notes = loadNotes();
    console.log(chalk.green.inverse('Your notes'));
    notes.forEach((note) => {
        console.log(chalk.blue.inverse(note.title));
    });
};

const readNote = (title) => {
    const notes = loadNotes();
    const found_note = notes.find((note) => note.title === title);
    if(found_note) {
        console.log(chalk.green.inverse("Title: " + found_note.title + ", body: " + found_note.body));
    } else {
        console.log(chalk.red.inverse("Note not found."));
    }
};

module.exports = {
    listNotes: listNotes,
    addNote: addNote,
    removeNote: removeNote,
    readNote: readNote
};