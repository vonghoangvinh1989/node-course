const https = require('https');

const url = 'https://api.darksky.net/forecast/9bc6634631db6ff5aaa2b13b27040755/40,-75?units=us';

const request = https.request(url, (response) => {
    let data = '';

    response.on('data', (chunk) => {
        data = data + chunk.toString();
        console.log(chunk);
    });

    response.on('end', () => {
        const body = JSON.parse(data);
        console.log(body);
    });
});

request.on('error', (error) => {
    console.log('An error', error);
});

request.end();