require('../task-manager/src/db/mongoose');
const Task = require('../task-manager/src/models/task');

// Task.findByIdAndDelete('5cebb82e7cc75972ac46222e').then((task) => {
//     console.log(task);
//     return Task.countDocuments({ completed: false });
// }).then((result) => {
//     console.log(result);
// }).catch((e) => {
//     console.log(e);
// });


const deleteTaskAndAcount = async (id) => {
    const task = await Task.findByIdAndDelete(id);
    const count = await Task.countDocuments({completed: false});
    return count;
};


deleteTaskAndAcount('5ceb5cee3971572c9cdfed5f').then((count) => {
    console.log(count);
}).catch((error) => {
    console.log(error);
});