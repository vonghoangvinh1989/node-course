require('../task-manager/src/db/mongoose');
const User = require('../task-manager/src/models/user');

// 5ceb5734989bd128569aac8d

// User.findByIdAndUpdate('5ceb5c2771947e2c0dd09b27', {
//     age: 1
// }).then((user) => {
//     console.log(user);
//     return User.countDocuments({ age: 1});
// }).then((result) => {
//     console.log(result);
// }).catch((e) => {
//     console.log(e);
// });

const updateAgeAndCount = async (id, age) => {
    const user = await User.findByIdAndUpdate(id, { age} );
    const count = await User.countDocuments({ age });
    return count; 
};

updateAgeAndCount('5ceb5c2771947e2c0dd09b27', 2).then((count) => {
    console.log(count);
}).catch((e) => {
    console.log(e);
});